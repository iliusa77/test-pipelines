<?php
 
require 'GumballMachine.php';
 
class GumballMachineTest extends PHPUnit_Framework_TestCase{

	public $GumballMachineInstance;
	
	public function setUp(){
		$this->GumballMachineInstance = new GumballMachine();
	}
	
	public function testIfWheelWorks(){
		$this->GumballMachineInstance->setGumballs(100);
		
		$this->GumballMachineInstance->turnWheel();
		
		$this->assertEquals(99, $this->GumballMachineInstance->getGumballs());
	}

}
?>