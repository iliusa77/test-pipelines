<?php 

class GumballMachine{
	private $gumballs;
	
	public function getGumballs(){
		return $this->gumballs;
	}
	
	public function setGumballs($amount){
		return $this->gumballs = $amount;
	}
	
	public function turnWheel(){
		return $this->setGumballs($this->getGumballs = 2);
	}

}

?>